﻿Shader "Custom/AnimatedMesh" {

	Properties {
		//_SingleTexture("Single Texture == 0", Range(0.0,1.0)) = 0.0
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_Tint("ScaledColor", Color) = (0.2,0.2,0.2,0.5)
		_AnimTex ("Albedo (RGB)", 2D) = "white" {}
		_AnimTextures ("AnimationTextures", 2DArray) = "" {}
		_VerticlMultiplier("Verticle Bone Texture Percent",Float) = 0.0
		_BoneDivisionMultiplier("Bone Division", Float) = 0.0

	    _PlaySpeed("Anim Speed", Float) = 1.0
		_OldMax("OldMaxScale", Float) = 1
		_OldMin("OldMinScale", Float) = -1
		_NewMax("NewMaxScale", Float) = 0.73
		_NewMin("NewMinScale", Float) = 0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		//#pragma vertex vert
		#include "UnityCG.cginc"
		#pragma multi_compile_instancing
		#pragma surface surf Standard fullforwardshadows vertex:vert
		// to use texture arrays we need to target DX10/OpenGLES3 which
        // is shader model 3.5 minimum
		#pragma target 3.5
		//
		UNITY_DECLARE_TEX2DARRAY(_AnimTextures);

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		half _Glossiness;
		half _Metallic;
	
		fixed4 _Color;
		float _TextureArrayIndex;
		//float _SingleTexture;
		sampler2D _AnimTex;
		float _OffsetX;
		float4 _Tint;
		float _VerticlMultiplier;
		float _BoneDivisionMultiplier;
		float _OldMax;
		float _OldMin;
		float _NewMax;
		float _NewMin;
		float _PlaySpeed;
		float newThresh;
		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_CBUFFER_START(Props)
			UNITY_DEFINE_INSTANCED_PROP(float, _TotalFramesInstance)
			UNITY_DEFINE_INSTANCED_PROP(float, _FramesPlaySpeed)
			UNITY_DEFINE_INSTANCED_PROP(int, _InstanceTextureIndex)
			UNITY_DEFINE_INSTANCED_PROP(float4, _Colors)
		UNITY_INSTANCING_CBUFFER_END

		struct appdata  {
			UNITY_VERTEX_INPUT_INSTANCE_ID
				fixed4 color : COLOR;
                fixed4 texcoord : TEXCOORD0;
				fixed4 texcoord1 : TEXCOORD1;
				fixed4 texcoord2 : TEXCOORD2;
                float4 vertex : POSITION;
				float3 normal : NORMAL;
				uint id : SV_VertexID;
				uint inst : SV_INSTANCEID;
        };

		float Scale(float OldMin, float OldMax, float NewMin, float NewMax, float OldValue)
			{

				float OldRange = (OldMax - OldMin);
				float NewRange = (NewMax - NewMin);
				float NewValue = (((OldValue - OldMin) * NewRange) / OldRange) + NewMin;

				return (NewValue);
			}

		// vertex position inputuint vid : SV_VertexID // vertex ID, needs to be uint
            void vert (inout appdata v)
            {
				UNITY_SETUP_INSTANCE_ID(v);
				// output funky colors based on vertex ID
                float f = (float)v.id;//Scale(0,506,0,1, (float)vid);
				float _TotalFrames = UNITY_ACCESS_INSTANCED_PROP(_TotalFramesInstance);
				_PlaySpeed = UNITY_ACCESS_INSTANCED_PROP(_FramesPlaySpeed);
				float divTotal = _TotalFrames/ _VerticlMultiplier;
				float boneDivTotal = _BoneDivisionMultiplier/ _VerticlMultiplier;
				float vertexTotal = boneDivTotal /_BoneDivisionMultiplier;
				//Find desired Float range of texture based on Max Frames.
				f++;
				//Update Offset for Playing animation
				//Set OffsetX
				_OffsetX = ((_Time.x*_PlaySpeed) - (1 * floor((_Time.x*_PlaySpeed)/1)));
				//Set Texturer
				float _TextureArrayIndex = UNITY_ACCESS_INSTANCED_PROP(_InstanceTextureIndex);

				#if !defined(SHADER_API_OPENGL)
					// sample from the array
					float3 uv;
					uv.xy = float2((_OffsetX*divTotal),(f/_VerticlMultiplier));
					uv.z = 0;
					
					//float4 animMap = UNITY_SAMPLE_TEX2DARRAY_LOD(_AnimTextures,uv,0.0);//tex2Dlod (_AnimTex, float4((_OffsetX*divTotal),(f/_VerticlMultiplier),0.0,0.0));
					float4 animMap = tex2Dlod(_AnimTex, float4((_OffsetX*divTotal), (f / _VerticlMultiplier), 0.0, 1.0));
					//Run Texture Array Logic
					//animMap = UNITY_SAMPLE_TEX2DARRAY_LOD(_AnimTextures, uv, 0.0);// samples a texture array with an explicit mipmap level.
					/*if(_SingleTexture > 0.5){
						//Run Texture Array Logic
						animMap = UNITY_SAMPLE_TEX2DARRAY_LOD(_AnimTextures,uv,0.0);// samples a texture array with an explicit mipmap level.
					} else {
						//Play Single Texture?
						animMap = tex2Dlod (_AnimTex, float4((_OffsetX*divTotal),(f/_VerticlMultiplier),0.0,1.0));
					}*/
					//Run Single Texture Logic
					//float4 animMap = tex2Dlod(_AnimTex, float4((_OffsetX*divTotal),(f/_VerticlMultiplier),0.0,0.0));
					//DeNormalize
					float x = Scale(_NewMin, _NewMax, _OldMin, _OldMax, animMap.x);
					float y = Scale(_NewMin, _NewMax, _OldMin, _OldMax, animMap.y);
					float z = Scale(_NewMin, _NewMax, _OldMin, _OldMax, animMap.z);
					//float w = Scale(_NewMin, _NewMax, _OldMin, _OldMax, animMap.w);
					float4 scaleVector = float4(x,y,z, 1.0f);
				#endif

				//Apply Changes
				v.color = scaleVector;
				v.vertex = scaleVector;
            }


		void surf (Input IN, inout SurfaceOutputStandard o) {
			_Color = UNITY_ACCESS_INSTANCED_PROP(_Colors);
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
