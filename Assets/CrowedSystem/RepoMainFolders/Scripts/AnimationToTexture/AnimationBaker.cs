﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

//using System.Diagnostics;
//This will Translate an Animation into a Texture.
public class AnimationBaker : MonoBehaviour
{
    //These two arrays should match. After Scaling.
    public Vector3[] desiredFirstFrameVertex;

    public bool writeUsingDirectPositions;//Means we will normalize direct values.
    public string textureName;
    public float oldMax = 9;
    public float oldMin = -9;
    public float newMax = 1;
    public float newMin = 0;

    public float animationBakeSpeed = 0.016f;
    //public Stopwatch timer;
    public float delay = 3;
    internal Mesh mesh;
    public string animationName;
    public Texture2D animations;
    public Animator animator;
    private bool generated = false;
    public SkinnedMeshRenderer skinnedMesh;
    private Mesh tempMesh;
    //For animation Texture Creations
    private int totalFrames = 78;

    [HideInInspector]
    public List<Vector3> bakedVertices = new List<Vector3>();
    [HideInInspector]
    public List<Vector3> bakedNormals = new List<Vector3>();

    void Start()
    {
        //Grab Animator for Animation Baking
        animator = gameObject.GetComponent<Animator>();
        generated = false;

        if (!skinnedMesh)
        {
            skinnedMesh = GetComponent<SkinnedMeshRenderer>();
            if (skinnedMesh)
            {
                mesh = skinnedMesh.sharedMesh;
            }
            //Fire Coroutine
            BakeAnimation();
        }
        else
        {
            mesh = skinnedMesh.sharedMesh;
            //Fire Coroutine
            BakeAnimation();
        }
    }

    private void UpdateMeshVerts()
    {
        tempMesh = new Mesh();
        skinnedMesh.BakeMesh(tempMesh);
        // don't use tempMesh.vertices; or tempMesh.normals; (creates memory)
        // lists are ok
        tempMesh.GetVertices(bakedVertices);
        tempMesh.GetNormals(bakedNormals);
    }

    void BakeAnimation()
    {
        Debug.Log("Bake Animation");
        //yield return new WaitForSeconds(delay);
        if (!generated)
        {
            //Go to Base Position of Animation
            animator.Play(animationName, 0, 0);
            //Get Current clip Length
            AnimatorStateInfo currInfo = animator.GetCurrentAnimatorStateInfo(0);
            totalFrames = Mathf.RoundToInt((currInfo.length) * 30);
            Debug.Log("How many frames are in the current Animation? " + totalFrames);
            animator.Update((float)0 / totalFrames);
            animator.speed = 0;
            //Update Verts for Mesh
            UpdateMeshVerts();
            //Get Mesh Vertex count
            int vertsInMesh = mesh.vertexCount;
            Debug.Log("VertsInMesh = Width: " + vertsInMesh + " TotalFrames = Height: " + totalFrames + " = Resolution of " + vertsInMesh + " * " + totalFrames);
            //Calculate Base Pose of Animation.
            //Iterate through all the Vertices and Store them into the a temporary index of Vector3[]
            Vector3[] baseVectors = new Vector3[bakedVertices.Count];//skinnedMesh.sharedMesh.vertices;
            baseVectors = bakedVertices.ToArray();
            desiredFirstFrameVertex = bakedVertices.ToArray();
            //Debug
            UnityEngine.Debug.Log("Clip Lenght: " + totalFrames);
            //Check Texture dimensions
            if (vertsInMesh < 1024 && totalFrames < 1024)
            {
                animations = new Texture2D(1024, 1024, TextureFormat.RGBA32, false);
            }
            else if (vertsInMesh < 2048 && totalFrames < 2048)
            {
                animations = new Texture2D(2048, 2048, TextureFormat.RGBA32, false);
            }
            else if (vertsInMesh < 4096 && totalFrames < 4096)
            {
                animations = new Texture2D(4096, 4096, TextureFormat.RGBA32, false);
            }
            else if (vertsInMesh < 8192 && totalFrames < 8192)
            {
                animations = new Texture2D(8192, 8192, TextureFormat.RGBA32, false);
            }
            else {
                Debug.LogError("IMPOSSIBLE ANIMATION CANNOT BE BAKED INTO TEXTURE!!!", this);
                //Cannot Continue
                //yield break;
            }
            //Setup Animations filter mode to Point
            animations.filterMode = FilterMode.Point;
            animations.wrapMode = TextureWrapMode.Clamp;
            //Update Mip Maps
            animations.Apply(true, false);
            //Total Frames
            //Iterate through X Pixels of Texture over total animation frames amount.
            for (int x = 0; x < totalFrames; x++)
            {
                animator.Play(animationName, 0, (float)x / totalFrames);
                animator.Update((float)x / totalFrames);
                animator.speed = 0;
                UpdateMeshVerts();
                //Grab current Vertices Location for this frame.
                Vector3[] currentVectors = new Vector3[bakedVertices.Count];
                currentVectors = bakedVertices.ToArray();

                //Now Iterate through all the Vertices at there current positions and encode into RGBA Pixel
                for (int y = 0; y < vertsInMesh; y++)
                {
                    //Vert in mesh RGBA Conversion.
                    //Vector3 currentVectPos = currentVectors[y];
                    Vector3 currentVectPos = currentVectors[y];//transform.TransformPoint(currentVectors[y]);
                    if (!writeUsingDirectPositions)
                    {
                        //Determine Direction Vector from Base -> current Frame Pos.
                        currentVectPos = currentVectors[y] - baseVectors[y];
                        //Normalize Value
                        float distance = currentVectPos.magnitude;
                        currentVectPos = currentVectPos / distance; // This is now the normalized direction.
                    }
                    //Scale to 0-1 Range from -1,1
                    //Normalize XYZ for RGB Conversion
                    float R = Scale(oldMin, oldMax, newMin, newMax, currentVectPos.x);//currentVectPos.x;//(currentVectPos.x + 1) / 2;
                    float G = Scale(oldMin, oldMax, newMin, newMax, currentVectPos.y);//currentVectPos.y;//(currentVectPos.y + 1) / 2;
                    float B = Scale(oldMin, oldMax, newMin, newMax, currentVectPos.z);//currentVectPos.z;//(currentVectPos.z + 1) / 2;
                    float A = Scale(oldMin, oldMax, newMin, newMax, currentVectPos.magnitude);//(vectorDir.magnitude + 1) / 2;

                    //Create Color
                    Color color = new Color(R, G, B, A);
                    //Color color = new Color(R, G, B, 0);
                    animations.SetPixel(x, y, color);

                    if (y == 0 && x == 0)
                    {
                        //Meaning the First Vectors of the Animation
                        Debug.Log("Previous: " + currentVectPos);
                        Debug.Log("R: " + R + " G: " + G + " B: " + B);
                        Debug.Log("De Normalized: " + new Vector3(Scale(newMin, newMax, oldMin, oldMax, color.r), Scale(newMin, newMax, oldMin, oldMax, color.g), Scale(newMin, newMax, oldMin, oldMax, color.b)));
                    }
                }
                //Store as baseVectors
                //yield return new WaitForSeconds(animationBakeSpeed);
            }

            //Write to Texture
            byte[] bytes = animations.EncodeToPNG();
            //Apply
            animations.Apply(false, false);//Apply Changes to Texture
            Debug.Log("Saved Texture to: " + Application.dataPath + "./"+textureName+".png");
            File.WriteAllBytes(Application.dataPath + "./"+textureName+".png", bytes);//Write Texture to Disk
        }
        generated = true;
        //End Coroutine
        //yield break;
    }

    public float Scale(float OldMin, float OldMax, float NewMin, float NewMax, float OldValue)
    {

        float OldRange = (OldMax - OldMin);
        float NewRange = (NewMax - NewMin);
        float NewValue = (((OldValue - OldMin) * NewRange) / OldRange) + NewMin;

        return (NewValue);
    }
}