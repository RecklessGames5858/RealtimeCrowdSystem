﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestDirectionVector : MonoBehaviour {

    public float vectorMagnitude = 1.0f;
    public Vector3 dirVector = new Vector3(0, 0, 0);
    public Transform ourTrans;

	// Use this for initialization
	void Start () {
        ourTrans = this.transform;
	}
	
	// Update is called once per frame
	void Update () {
        //Translate to Local Space
        Vector3 localDirVector = dirVector;//ourTrans.TransformPoint(dirVector);
        //Move object
        ourTrans.position += (localDirVector - ourTrans.position);
        //ourTrans.Translate((localDirVector - ourTrans.position));
	}
}
