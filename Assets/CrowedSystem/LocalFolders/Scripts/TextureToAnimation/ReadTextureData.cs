﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReadTextureData : MonoBehaviour {
    public Vector3[] returnedValue;
    [Header("Debug Color Values")]
    public bool showVerticesOrderOnMesh = false;
    public float sphereSize = 0.1f;
    public Color debugColor = Color.red;
    public float colorRange = 1;
    [Header("Scale Values")]
    public float oldMax = 1;
    public float oldMin = -1;
    public float newMax = 0.73f;
    public float newMin = 0;

    public float frameSpeed = 0.25f;
    public float _Scale;
    private int x = 0;
    public int y = 0;
    public int maxFrames = 78;
    public Texture2D animTex;
    public Color[] pixels;
    private Vector3[] meshVerts;
    public MeshFilter skinnedMesh;
    public Vector3[] verts;
    public Vector3[] normals;

    // Use this for initialization
    void Start() {
        if (!showVerticesOrderOnMesh)
        {
            StartCoroutine(ReadVertexData());
        }
        else {
            //Just show the Vertices.
            StartCoroutine(ShowVerticesOrder());
        }
    }

    GameObject[] visualVerts;
    //This function will iterate through the static mesh vertices data and visualize the Points in there Local Order
    IEnumerator ShowVerticesOrder() {
        //Grab Vertices and Normals from Mesh
        verts = skinnedMesh.mesh.vertices;
        normals = skinnedMesh.mesh.normals;
        //Initialize
        visualVerts = new GameObject[verts.Length];
        //Fill
        //Now iterate through vertices and Createa a Spahere with a number to match its order for debuging.
        for (int i = 0; i < verts.Length; i++) {
            //Determine 
            Vector3 pos = new Vector3(verts[i].x,verts[i].z,-verts[i].y);
            GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphere.name = i.ToString();
            sphere.transform.position = pos;
            sphere.transform.localScale = new Vector3(sphereSize, sphereSize, sphereSize);
            visualVerts[i] = sphere;
        }

        //Now Animate and Move Visuals With Animation
        yield return new WaitForSeconds(frameSpeed);
        StartCoroutine(ReadVertexData());
        //end coroutine
        yield break;
    }

    IEnumerator ReadVertexData() {
        if (animTex != null) {
            //How many pixels are in the V axis of the Texter?
            int textureHeight = animTex.height;//V of UV
            int textureWidth = animTex.width;//U of UV
            Debug.Log("Texture Width: " + textureWidth + " Texture Height: " + textureHeight);
            //Grab Index locations.
            verts = skinnedMesh.mesh.vertices;
            normals = skinnedMesh.mesh.normals;
            returnedValue = new Vector3[verts.Length];
            List<Vector3> lastPositions = new List<Vector3>();
            //Set Initial Postions
            for (int i = 0; i < verts.Length; i++) {
                lastPositions.Add(verts[i]);
            }
            //Create manipulated Array.
            for (x = 0; x < maxFrames; x++)
            {
                //There are 507 pixels. :)
                pixels = animTex.GetPixels(x, y, 1, textureWidth);
                //Debug.Log("Pixels Length: " + pixels.Length);
                //Iterate through the pixels matching to verts :)
                for(int i = 0; i < verts.Length; i++)
                {
                    //Debug.Log("Pixel RGBA: " + ((pixels[i].r*2f)-1f)+ ((pixels[i].g*2f)-1f)+((pixels[i].b*2)-1f) + pixels[i].a);

                    //Return Exact Value
                    float x = Scale(newMin, newMax, oldMin, oldMax, pixels[i].r);
                    float y = Scale(newMin, newMax, oldMin, oldMax, pixels[i].g);
                    float z = Scale(newMin, newMax, oldMin, oldMax, pixels[i].b);
                    //float w = Scale(newMin, newMax, oldMin, oldMax, pixels[i].a);
                    //
                    Vector3 dirVec = new Vector3(x, y, z);//new Vector3(((x * 2f) - 1f), ((y * 2f) - 1f), ((z * 2f) - 1f));
                    if (lastPositions[i] != Vector3.zero)
                    {
                        //Draw Line
                        Debug.DrawLine(new Vector3(lastPositions[i].x,lastPositions[i].z, -lastPositions[i].y), new Vector3(dirVec.x,dirVec.z,-dirVec.y), debugColor * colorRange);
                        //Update Last Positions
                        lastPositions[i] = dirVec;
                    }
                    else {
                        //Update Last Positions
                        lastPositions.Add(dirVec);
                    }

                    if (showVerticesOrderOnMesh)
                    {
                        //Update Stored Visual
                        visualVerts[i].transform.position = verts[i];//Convert to World Space from Local Space
                    }
                    //Debug.Log("Before Change: " + verts[i]);
                    verts[i] = dirVec;

                    if (showVerticesOrderOnMesh)
                    {
                        //Update Stored Visual
                        //visualVerts[i].transform.position = verts[i];//Convert to World Space from Local Space
                    }
                    returnedValue[i] = dirVec;
                }
                //Now add to Mesh Data
                //Create Mesh
                Mesh tempMesh = new Mesh();
                tempMesh = skinnedMesh.mesh;
                tempMesh.vertices = verts;
                tempMesh.RecalculateBounds();
                tempMesh.RecalculateNormals();
                skinnedMesh.mesh = tempMesh;

                yield return new WaitForSeconds(frameSpeed);
                //Now that we have the Directions.
            }
            /*for (int y = 0; y < textureHeight; y++) {
                //Check
                for (int x = 0; x < textureWidth; x++)
                {
                    pixels = animTex.GetPixels(x, y, 1, textureWidth);
                    //Getting a single Pixel is wastefull.
                    //Get Pixel Color
                    Vector3 pos = new Vector3(pixels[0].r, pixels[0].g, pixels[0].b);
                    //Show me the position.
                    CreateObject(count.ToString(), pos, new Vector3(0.2f, 0.2f, 0.2f));
                    count++;
                }
            }*/

            /*meshVerts = new Vector3[pixels.Length];
            //Iterate through first column.
            for (int i = 0; i < pixels.Length; i++) {
                Vector3 pos = new Vector3(pixels[i].r, pixels[i].g, pixels[i].b);
                meshVerts[i] = pos;
                //Show me the position.
                CreateObject(i.ToString(), pos, new Vector3(0.2f,0.2f,0.2f));
                //Debug.Log("R: "+pixels[i].r+" G: "+ pixels[i].g+" B: " + pixels[i].b+" A: " +pixels[i].a);
            }*/
            //Read RGBA floats
            //animTex.GetPixels(texcoord1.x, v.texcoord1.y + _OffsetY, 1, 1);
            //Lets Create some points of Data.

            //float4 animMap = tex2Dlod(_AnimTex, float4(v.texcoord1.x, v.texcoord1.y + _OffsetY, 0, 0));
            //float4 dirVec = float4((animMap.r * 2.0) - 1.0, (animMap.g * 2.0) - 1.0, (animMap.b * 2.0) - 1.0, 0.0);
            //v.vertex += dirVec * animMap.a * _Scale;
            //StartCoroutine again
            StartCoroutine(ReadVertexData());
        }
	}

    GameObject CreateObject(string name, Vector3 pos, Vector3 scale) {
        GameObject newObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
        newObject.transform.position = pos;
        newObject.name = name;
        newObject.transform.localScale = scale;
        return newObject;
    }

    //Normalize from 0-1
    /*float Normalize(float value, float min, float max)
    {
        return (value - min) / (max - min);
    }

    float DeNormalize(float value, float min, float max)
    {
        return (value * (max - min) + min);
    }*/

    public float Scale(float OldMin, float OldMax, float NewMin, float NewMax, float OldValue)
    {

        float OldRange = (OldMax - OldMin);
        float NewRange = (NewMax - NewMin);
        float NewValue = (((OldValue - OldMin) * NewRange) / OldRange) + NewMin;

        return (NewValue);
    }
}
