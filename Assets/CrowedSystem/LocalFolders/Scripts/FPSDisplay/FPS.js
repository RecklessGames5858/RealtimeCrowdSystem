// Attach this to a GUIText to make a frames/second indicator.
//
// It calculates frames/second over each updateInterval,
// so the display does not keep changing wildly.
//
// It is also fairly accurate at very low FPS counts (<10).
// We do this not by simply counting frames per interval, but
// by accumulating FPS for each frame. This way we end up with
// correct overall FPS even if the interval renders something like
// 5.5 frames.
import System.Collections.Generic;
var updateInterval = 0.5;
var sampleCap = 250;//The Amount of FPS Samples to take for determining FPS Avg.

private var accum = 0.0; // FPS accumulated over the interval
private var frames = 0; // Frames drawn over the interval
private var timeleft : float; // Left time for current interval

private var delay = true;
private var low : float;
private var high : float;
private var averageFPS : float;

//Used to determine proper Average over Time.
private var fpsList : List.<float>;

function Start()
{
    if( !GetComponent.<GUIText>() )
    {
        print ("FramesPerSecond needs a GUIText component!");
        enabled = false;
        return;
    }
    timeleft = updateInterval; 
	//Initilize List
	fpsList = new List.<float>(0);

    CountMinMax();
}

function CountMinMax(){

    //Set Min and Max.
    low = Mathf.Infinity;
    high = 0;
    yield WaitForSeconds(5);

    delay = false;
}
 
function Update()
{
    timeleft -= Time.deltaTime;
    accum += Time.timeScale/Time.deltaTime;
    ++frames;
 
    // Interval ended - update GUI text and start new interval
    if( timeleft <= 0.0 )
    {
        // display two fractional digits (f2 format)
        var currentFPS = (accum/frames);
		//Add CurrentFPS to List
		//Only store 100 numbers. 
		/*if(fpsList.length >= 100){
			fpsList.Add(currentFPS);
			//Remove First
			fpsList.RemoveAt(0);
		} else {
			fpsList.Add(currentFPS);
		}*/

        if(!delay){
            if(low > currentFPS){
                low = currentFPS;
            }
            if(high < currentFPS){
                high = currentFPS;
            }
			//Add to List
			fpsList.Add(currentFPS);
			//Sort List by Value(Low - High)
			fpsList.Sort();
			//Cap Memory List to 250 samples.
			if(fpsList.Count > sampleCap){
				//Remove from the center of List
				fpsList.RemoveAt(fpsList.Count/2);
			}
			//determine Average.
			if(fpsList.Count > 1){
				averageFPS = 0;
				for (var i = 0; i < fpsList.Count; i++){
					averageFPS+=fpsList[i];
				}
				averageFPS = averageFPS/fpsList.Count;
			}
        }
        GetComponent.<GUIText>().text = "FPS - " + currentFPS.ToString("f2") +" Average: "+averageFPS +" Low: "+low.ToString("f2")+" High: "+high.ToString("f2");
        timeleft = updateInterval;
        accum = 0.0;
        frames = 0;
    }
}