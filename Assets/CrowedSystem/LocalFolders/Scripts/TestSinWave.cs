﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSinWave : MonoBehaviour {

    public float explicitValue = 1;
    public float runTime = 5f;
    public float _PlaySpeed = 1;
    //private float sinValue = 0;
    public float currentTime = 0;

	// Use this for initialization
	void Start () {
        StartCoroutine(SinWave());
	}

    private float startTime = 0;

    IEnumerator SinWave() {
        currentTime = 0;
        //Restart Timer
        startTime = Time.time;

        while ((Time.time - startTime) <= runTime) {
            //Check
            currentTime = Time.time;//Constantly goes up.
            //Check if Value is Float or int
            float decimalValue = (Time.time % 1);
            
            if (decimalValue == 0){
                //Int   
                Debug.Log(decimalValue+" Zero Starting Over");
            } else {
                //Float
                Debug.Log(decimalValue + " 0-1 float Value, animating");
            }
            /*currenTime = Time.time;
            if (currenTime > 1.57) {
                currenTime = 0;
            }
            sinValue = Mathf.Sin(currenTime) * _PlaySpeed;//sin(_Time.x * _PlaySpeed);
            //Update SinValue to Stay in the 0->1 Range. So the 0-> -1 range is flipped to 0->1
            if (sinValue < 0.0) {
                sinValue = Mathf.Abs(sinValue);
            }
            //Loop through Logic
            Debug.Log("Sin Wave: "+sinValue.ToString() +" CurrentTime: "+ Time.time);*/
            yield return new WaitForEndOfFrame();
        }
        yield break;
    }
}
