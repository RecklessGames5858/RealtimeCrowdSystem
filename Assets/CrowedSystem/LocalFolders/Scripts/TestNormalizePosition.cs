﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestNormalizePosition : MonoBehaviour {

    public float origValueToBeNormalized = 12;
    public float oldMax = 9;
    public float oldMin = -9;
    public float newMax = 1;
    public float newMin = 0;
    public float normalizedValue = 0;
    public float returnedValue = 0;
    public Transform ourTrans;


	// Use this for initialization
	void Start () {
        //get original value
        //Debug.Log("orignal: " + ourTrans.position);
        Debug.Log("original: "+ origValueToBeNormalized);
        normalizedValue = origValueToBeNormalized;
        normalizedValue = Scale(oldMin, oldMax, newMin, newMax, origValueToBeNormalized); //Normalize(NormalizedValue, min, max);
        Debug.Log("Normalized: " + normalizedValue);
        returnedValue = Scale(newMin, newMax, oldMin, oldMax, normalizedValue);// DeNormalize(NormalizedValue, min, max);
        Debug.Log("DeNormalized: " + returnedValue);
        /*Vector3 pos = ourTrans.position;
        //Normalized Value
        float R = Normalize(pos.x, 0, 1);//(pos.x + 1) / 2;
        float G = Normalize(pos.y, 0, 1);//(pos.y + 1) / 2;
        float B = Normalize(pos.z, 0, 1);//(pos.z + 1) / 2;
        Vector3 normalizedPos = new Vector3(R, G, B);
        Debug.Log("NormalizedPos: "+normalizedPos);
        //Return to Unormalized
        Vector3 unNormed = Vector3.zero;
        unNormed.x = DeNormalize(R,-900,999);//(R * 2.0f) / 1f;
        unNormed.y = DeNormalize(G, -900, 999);//(R * 2.0f) / 1f;
        unNormed.z = DeNormalize(B, -900, 999);//(R * 2.0f) / 1f;*/
        //Debug.Log("Returned From Normalized : " + unNormed);
        //float A = (pos.magnitude + 1) / 2;
    }
        /*$int = 12;
        $max = 20;
        $min = 10;

        $normalized = normalize($int, $min, $max); // 0.2
        $denormalized = denormalize($normalized, $min, $max); //12*/

    //Normalize from 0-1
    /*float Normalize(float value, float min, float max) {
        return (value - min) / (max - min);
    }


    float DeNormalize(float value, float min, float max)
    {
        return (value * (max - min) + min);
    }*/

    public float Scale(float OldMin, float OldMax, float NewMin, float NewMax, float OldValue)
    {

        float OldRange = (OldMax - OldMin);
        float NewRange = (NewMax - NewMin);
        float NewValue = (((OldValue - OldMin) * NewRange) / OldRange) + NewMin;

        return (NewValue);
    }
    /*function denormalize($normalized, $min, $max)
    {
	$denormalized = ($normalized * ($max - $min) + $min);
        return $denormalized;
    }*/
}
