﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTypeTester : MonoBehaviour {

    [Header("Debug Values")]
    public float hSliderValue = 500;
    public float sliderMax = 10000;
    public int spawnAmount = 0;
    public float spacing = 1.6f;

    [Header("GUI Values")]
    public float xRect = 0;
    public float yRect = 0;
    public float width = 10;
    public float height = 10;

    [Header("GPU Instancing")]
    public bool useGPUInstancing = false;//If true use gpu instance

    [Header("Prefabs")]
    public GameObject skinnedPrefab;//The Object that will hold all the skinned mesh renderers with animators
    public GameObject meshRendererPrefab;//The Prefab we will send to the GPU for Instacing.
    private Transform holder;//The Transform we will nest the Prefabs into

    //Monobehavior Start Function
    private void Start()
    {
        StartCoroutine(SpawnUnits());
    }

    //Reuse MaterialBlock for setting GPU Instaced Variables (Color etc)
    //Set Property
    MaterialPropertyBlock props;
    //GPU Instancing Mesh Renderer
    MeshRenderer[] renderers;
    // Use this for initialization of Spawning desired units
    IEnumerator SpawnUnits() {
        //Clear Old Holder if applicable
        if (holder != null) {
            Destroy(holder.gameObject);
        }


        holder = new GameObject("Holder").transform;
        //Initialize Grid (Exact desired number may not be possible so we will use the closest squared value)
        float sqr = Mathf.Sqrt(hSliderValue);
        //split squared root
        int xMax = (int)sqr;
        int yMax = (int)sqr;
        //Clear Counter for new spawning
        spawnAmount = 0;

        //Do we want to use GPU instacing on a MeshRenderer with Shader Animation or Use the Skinned Mesh Renderer with Unity Animator for animation.
        if (useGPUInstancing)
        {
            //use GPU instacing on a MeshRenderer with Shader Animation
            for (int x = 0; x < xMax; x++)
            {
                for (int y = 0; y < yMax; y++) {
                    //Create GameObjects that hold Mesh Renderers
                    GameObject unit = Instantiate(meshRendererPrefab, new Vector3(x*spacing, 0, y*spacing), Quaternion.Euler(0.0f, Random.Range(0.0f, 360.0f), 0.0f), holder);
                    spawnAmount++;

                    //Create Property Blocks for Instanced Variables
                    props = new MaterialPropertyBlock();
                    renderers = unit.GetComponentsInChildren<MeshRenderer>();
                    //Set Instance Property _Colors
                    float r = Random.Range(0f, 1.0f);
                    float g = Random.Range(0f, 1.0f);
                    float b = Random.Range(0f, 1.0f);
                    //Set color
                    props.SetColor("_Colors", new Color(r, g, b, 1f));
                    //0 == walk, 1 = convo 1 2 = convor3
                    /*int randomInt = Mathf.RoundToInt(Random.Range(0.0f, 2.0f));
                    if (randomInt < 1)
                    {
                        //Play Walk Frames
                        props.SetFloat("_TotalFramesInstance", 86f);
                        props.SetFloat("_FramesPlaySpeed", 6f);
                    }
                    else if (randomInt < 2)
                    {
                        //Play Idea Frames
                        props.SetFloat("_TotalFramesInstance", 899f);
                        props.SetFloat("_FramesPlaySpeed", 0.6f);
                    }
                    else {
                        //Play Third Convo
                        props.SetFloat("_TotalFramesInstance", 866f);
                        props.SetFloat("_FramesPlaySpeed", 0.6f);
                    }*/
                    //Play Idea Frames
                    props.SetFloat("_TotalFramesInstance", 1215f);
                    props.SetFloat("_FramesPlaySpeed", 0.6f);
                    //Set Toal Frames based on the int;
                    //props.SetFloat("_InstanceTextureIndex", randomInt);
                    for (int i = 0; i < renderers.Length; i++)
                    {
                        //Set to Renderer
                        renderers[i].SetPropertyBlock(props);
                    }
                    //yield return new WaitForEndOfFrame();
                }
            }
        }
        else {
            //Use the Skinned Mesh Renderer with Unity Animator
            for (int x = 0; x < xMax; x++)
            {
                for (int y = 0; y < yMax; y++)
                {
                    Instantiate(skinnedPrefab, new Vector3(x, 0, y), Quaternion.Euler(0.0f, Random.Range(0.0f, 360.0f), 0.0f), holder);
                    spawnAmount++;
                }
            }
        }
        yield return new WaitForEndOfFrame();
    }

    //Monobehaivor Frame Update function
    private void Update()
    {
        //User Input Catch when inside a standalone
        //Escape
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }

    //Show GUI
    void OnGUI()
    {
        //Update Desired Spawn Amount Slider in GUI
        hSliderValue = Mathf.RoundToInt(GUI.HorizontalSlider(new Rect(Screen.width/xRect, Screen.height/yRect, width, height), hSliderValue, 0, sliderMax));
        //Display Value
        GUI.Label(new Rect(xRect+(xRect*1.6f), yRect, width, height), "Desired Spawn Amount: "+hSliderValue.ToString());
        GUI.Label(new Rect(xRect + (xRect * 1.6f), yRect * 1.6f, width, height), "Current Amount: " + spawnAmount);
        //Check for Spawn Button Input
        if (GUI.Button(new Rect(xRect, yRect + height, 160, 60), "Spawn"))
            StartCoroutine(SpawnUnits());

        //Toggle Button for using GPU Instacing
        useGPUInstancing = GUI.Toggle(new Rect(xRect, yRect+(height*3), 200, 30), useGPUInstancing, "Use GPU Instacing");

    }
}