﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;
using Debug = UnityEngine.Debug;

namespace Crowds {
    [System.Serializable]
    //This will hold all the Data for the Individual Bulk
    public class Bulk {
        //Needs to know its AI State. (Low, Puppet, Auto) == Low Res, Puppet, Autonomous
        [Header("ID")]
        public int bulkID;
        public enum BulkState { Low, Puppet, Auto }
        public Transform ourTrans;
        private Material mat;
        //Add new states here
        [Header("AI State")]
        public BulkState curState = BulkState.Low; // The current State of the Bulk for LODing
        [Header("Position Data")]
        public Vector3 pos;//The current Position of the Bulk.

        //Initalizer Requires ID and position.
        public Bulk(int newID, Vector3 newPosition, Transform holder, GameObject prefab, Quaternion rotation) {
            bulkID = newID;
            pos = newPosition;
            //GameObject newBulk = GameObject.CreatePrimitive(PrimitiveType.Cube);
            GameObject newBulk = GameObject.Instantiate(prefab, newPosition, rotation, holder);
            ourTrans = newBulk.transform;
            ourTrans.position = pos;
            ourTrans.parent = holder;
            newBulk.GetComponent<Renderer>().sharedMaterial.enableInstancing = true;
            //Grab GameObject material
            mat = newBulk.GetComponent<Renderer>().material;
        }

        //Color Code Red = auto, Orange = puppet, Blue = Low
        //Takes a State
        public void UpdateState(BulkState newState) {
            curState = newState;
            //Now based on the State. We will change to the Proper Looks.
            switch (curState) {
                case BulkState.Low:
                    //Blue
                    mat.color = Color.blue;
                    break;
                case BulkState.Puppet:
                    //Blue
                    mat.color = Color.yellow;
                    break;
                case BulkState.Auto:
                    mat.color = Color.red;
                    break;
            }
        }
    }

    //This will Control the LOD's and Logic of all the Bulks Contained in this Shepherd. This is a Actual Entity that lives in the Scene placed by Level Designers.
    public class Shepherd : MonoBehaviour {
        [Header("LOD Distances")]
        public GameObject prefab;
        public Stopwatch timer;

        public float yieldDist = 0.1f;
        public float puppetDistance;//how close puppet <= 40 its a Puppet Shepherd Controlled.
        public float autoDistance;// how close auto <= 12 Its Autonomous Shepherd ID Referenced but NOT Controlled.

        public int maxBulks;//The Maximum Amount of Spawned Bulks this shepherd will control.
        //Create and maintain a Bulk Crowd.
        public Bulk[] bulks;
        public Transform player;
        private Transform ourTrans;

        private void Start()
        {
            ourTrans = this.transform;
            Initialize();
        }

        //This function will initialize a Shepherd Crowd
        public void Initialize() {
            int count = 0;
            //Iterate through grid positions and create bulk data holding that position.
            //Initialize Grid
            float sqr = Mathf.Sqrt(maxBulks);
            //split squared root
            int xMax = (int)sqr;
            int zMax = (int)sqr;
            int halfX = xMax / 2;
            int halfZ = zMax / 2;
            bulks = new Bulk[xMax*zMax];

            Vector3 cenPos = ourTrans.position;

            for (int x = 0; x < xMax; x++)
            {
                for (int z = 0; z < zMax; z++)
                {
                    Vector3 newPos = new Vector3((cenPos.x+x) - halfX, cenPos.y, (cenPos.z+z) - halfZ);
                    
                    //Create Bulk Data one time.
                    Bulk newBulk = new Bulk(count, newPos, ourTrans, prefab, Quaternion.Euler(0.0f, Random.Range(0.0f, 360.0f), 0.0f));
                    //Add to Array
                    bulks[count] = newBulk;
                    count++;
                }
            }
            StartCoroutine(UpdateBulks());
        }
        //Grab Player Position.
        Vector3 playerPos;// player.position;
        Bulk tmpBulk = null;
        float dist = Mathf.Infinity;
        //
        private bool isOn = true;
        //Loop through and update Every Bulk state based on Distance to Player.
        IEnumerator UpdateBulks()
        {
            if (isOn) {
                //ctr
                timer = new Stopwatch();

                timer.Start();
            }
            while (true)
            {
                //Grab Player Position.
                playerPos = player.position;
                tmpBulk = null;
                dist = Mathf.Infinity;
                //Iterate and test dist bulks.
                for (int i = 0; i < bulks.Length; i++)
                {
                    //Reference value
                    tmpBulk = bulks[i];
                    //Test Distance to Player
                    //dist = Vector3.Distance(playerPos, tmpBulk.pos);
                    Vector3 offset = tmpBulk.pos - playerPos;
                    dist = offset.x * offset.x + offset.y * offset.y + offset.z * offset.z;//offset.sqrMagnitude;
                    
                    //lowDistance;//If its distance is > 40 its a low res Shepherd controlled.
                    //puppetDistance;//how close puppet <= 40 its a Puppet Shepherd Controlled.
                    //autoDistance;// how close auto <= 12 Its Autonomous Shepherd ID Referenced but NOT Controlled.
                    //Set Bulk sate based on Dist.
                    if (dist <= autoDistance * autoDistance)
                    {
                        //Is it already Auto?
                        //This is an Auto
                        if (tmpBulk.curState != Bulk.BulkState.Auto)
                            bulks[i].UpdateState(Bulk.BulkState.Auto);
                        //Debug.DrawLine(playerPos + (Vector3.up * 1.6f), tmpBulk.pos, Color.red);
                    }
                    else if (dist <= puppetDistance * puppetDistance)
                    {
                        //This is a Puppet
                        if(tmpBulk.curState != Bulk.BulkState.Puppet)
                            bulks[i].UpdateState(Bulk.BulkState.Puppet);
                        //Debug.DrawLine(playerPos + (Vector3.up * 1.6f), tmpBulk.pos, Color.yellow);
                    }
                    else
                    {
                        //This is a Low Res
                        if(tmpBulk.curState != Bulk.BulkState.Low)
                            bulks[i].UpdateState(Bulk.BulkState.Low);
                        //Debug.DrawLine(playerPos+(Vector3.up * 1.6f), tmpBulk.pos, Color.blue);
                    }
                }
                if (isOn) {
                    isOn = false;
                    timer.Stop();
                    Debug.Log(timer.Elapsed);
                    //(timer % 60).ToString("00);
                }
                yield return new WaitForEndOfFrame();
            }
        }

        //Returns Distance
        float ReturnDistance(Vector3 firstPosition, Vector3 secondPosition) {

            Vector3 heading;
            float distance;
            //Vector3 direction;
            //float distanceSquared;

            heading.x = firstPosition.x - secondPosition.x;
            heading.y = firstPosition.y - secondPosition.y;
            heading.z = firstPosition.z - secondPosition.z;

            //distanceSquared = heading.x * heading.x + heading.y * heading.y + heading.z * heading.z;
            distance = Mathf.Sqrt(heading.x * heading.x + heading.y * heading.y + heading.z * heading.z);

            //direction.x = heading.x / distance;
            //direction.y = heading.y / distance;
            //direction.z = heading.z / distance;
            
            return distance;
        }
    }
}
